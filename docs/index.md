# Introduction

_User-configurable, cross-platform Tk GUI
for opening predefined search URLs in a web browser._


!!! caution "Tkinter is required"

    In some Linux distributions,
    Tkinter does not come with python by default,
    but has to be installed separately, eg. in Debian:

        sudo apt install python3-tk



## Installation

!!! note ""

    Using a [virtual environment](https://packaging.python.org/en/latest/guides/installing-using-pip-and-virtual-environments/#creating-a-virtual-environment)
    is strongly recommended.


You can install the [package from PyPI](https://pypi.org/project/search-helper/):

```bash
pip install --upgrade search-helper
```


## Usage

!!! note ""

    Please note that the module name contains an underscore instead of a dash,
    in contrast to the package name.

Choose the configuration file via a dialog:

```bash
python -m search_helper
```

Specify a configuration file on the command line:

```bash
python -m search_helper configfile.yaml
```

Use one of the shipped example configuration files:

```bash
python -m search_helper example:abstract
python -m search_helper example:debian
```


## User interface

![UI on Windows using example.yaml](examples/example-screenshot.png)

When you have entered a search string, you can open it in the selected
category or categories using the "Open" button or the **Return** key.
This will open the URLs of each category in webbrowser tabs.
If possible, each category is opened in a separate browser window.

The first up to 12 category selections can be toggled using the function keys
as displayed (**F1** through **F12** from top down).

Pressing **Escape** or clicking the "Quit" button exits the program.

You can delete the search term by pressing **Ctrl-D** or by clicking
the "Clear" button. **Ctrl-X** will copy the search term to the clipboard
and then clear the search term entry field.

For single-URL categories, the "Copy URL" button will copy a URL
(which is generated from the search term in combination with the category
written before the button) to the clipboard.

Categories with multiple URLs have a "List URL names" button instead.
Clicking on that button will open a popup window containing a list of all
URL names/identifiers that belong to the category written before the button.

Other key combinations selecting multiple categories can be configured
in the configuration files. In the example above
(which uses the abstract example file), that is **Ctrl-Shift-A**
selecting the first and third category.
