# Changelog

## v1.2.3

- Executable paths support environment variable expansion


## v1.2.2

- Added Changelog
- Updated screenshot


## v1.2.1

Clickable links in info dialogs


## v1.2.0

- Added translations:
    - French
    - German
- Embedded examples


## v1.1.92

Added documentation on GitLab pages


## v1.1.91

Initial upload to PyPI, split up the original project
(<https://github.com/blackstream-x/searchhelper/releases/tag/v1.0.0>)
into multiple modules and changed config file loading to use
[serializable-trees](https://pypi.org/project/serializable-trees/).

